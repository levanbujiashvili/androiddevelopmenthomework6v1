package com.example.davaleba6

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import kotlinx.android.synthetic.main.fragment_button.*

/**
 * A simple [Fragment] subclass.
 */
class buttonFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val idk = inflater.inflate(R.layout.fragment_button, container, false)
        val addButton : Button = idk.findViewById<Button>(R.id.addButton)
        addButton.setOnClickListener(){
            val intent = Intent(this, NewItemTemplate::class.java)
            startActivity(intent)
        }
        return idk
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        addButton.setOnClickListener(){
            val intent = Intent(this, NewItemTemplate::class.java)
            startActivity(intent)
        }
        init()
    }
    private fun init() {
        addButton.setOnClickListener() {
            val intent = Intent(this, NewItemTemplate::class.java)
            startActivity(intent)
        }
    }


}
