package com.example.davaleba6

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.fragment_button.*

class MainActivity : AppCompatActivity(), View.OnClickListener {
    override fun onClick(p0: View?) {
        when (p0!!.id) {
            R.id.addButton -> {
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }

    private fun init(){
        addButton.setOnClickListener(){
            val intent = Intent(this, NewItemTemplate::class.java)
            startActivity(intent)
        }
        addFirstFragment()
        addSecondFragment()
    }
    private fun addFirstFragment(){
        val transaction = supportFragmentManager.beginTransaction()
        val recyclerFragment = RecyclerFragment()
        transaction.replace(R.id.firstContainer, recyclerFragment)
        transaction.commit()
    }
    private fun addSecondFragment(){
        val transaction1 = supportFragmentManager.beginTransaction()
        val buttonFragment = buttonFragment()
        transaction1.replace(R.id.secondContainer, buttonFragment)
        transaction1.commit()
    }
}
